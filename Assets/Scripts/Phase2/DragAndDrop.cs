﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
using UnityEngine.EventSystems;
using UnityEngine.WSA;


public class DragAndDrop : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler,
    IDropHandler
{
    [SerializeField] private Canvas _canvas;
    private IDragHandler _dragHandlerImplementation;
    private RectTransform _rectTransform;
    private CanvasGroup _canvasGroup;
    public bool droped;
    public float random;
    public float speed;
    public Vector3 newPosition;
    public float timer = 2;
    public Vector3 origin;

    private void Awake()
    {
        _rectTransform = GetComponent<RectTransform>();
        _canvasGroup = GetComponent<CanvasGroup>();
        origin = transform.position;
    }

    public void Update()
    {
        newPosition = new Vector3(random, transform.position.y, transform.position.z);

        random = Random.Range(1f, 10f);
        if (!droped)
        {
            transform.position = Vector3.Lerp(transform.position, newPosition, Time.deltaTime * speed);

            timer = timer - Time.deltaTime;
        }

        if (timer <= 0)
        {
            droped = true;
            transform.position = Vector3.Lerp(transform.position, origin, Time.deltaTime * speed);
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        Debug.Log("OnBeginDrag");
        // Feedback when bone is draging
        _canvasGroup.alpha = 0.6f;
        // Desactivate raycast
        _canvasGroup.blocksRaycasts = false;

        droped = true;
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (_dragHandlerImplementation != null)
        {
            _dragHandlerImplementation.OnDrag(eventData);
        }

        _rectTransform.anchoredPosition += eventData.delta / _canvas.scaleFactor;

        Debug.Log("OnDrag");
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Debug.Log("OnEndDrag");
        _canvasGroup.alpha = 1f;
        // Acivate raycast
        _canvasGroup.blocksRaycasts = true;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("OnPointerDown");
    }

    public void OnDrop(PointerEventData eventData)
    {
        Debug.Log("OnDrop");
    }
}