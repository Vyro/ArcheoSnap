﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BoneSlot : MonoBehaviour, IDropHandler
{
    private LevelManager levelManager;

    void Start()
    {
        levelManager = FindObjectOfType<LevelManager>();
        if (levelManager == null)
        {
            Debug.LogError("No levelManager");
        }
    }
    
    public void OnDrop(PointerEventData eventData)
    {
        if (eventData.pointerDrag != null)
        {
            // Snap the bone into the slot
            eventData.pointerDrag.GetComponent<RectTransform>().anchoredPosition =
                GetComponent<RectTransform>().anchoredPosition;
            levelManager.SetNbBonesStored(levelManager.GetNbBonesStored() + 1);
        }

        if (levelManager != null)
        {
            if (levelManager.GetNbBonesStored() == levelManager.GetNbBonesToStore())
            {
                Debug.Log("caca");
                // Level done
                levelManager.SetIsLevelDone(true);
                GameManager.Instance.Victory();
            }
        }
    }
}
