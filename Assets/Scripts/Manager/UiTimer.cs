﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiTimer : MonoBehaviour
{
    public Text timerText; 
    public bool playing = true;
    [SerializeField] private float timer = 7f;

    void Start()
    {
        
    }

    void Update () {
        
        if (timer <= 0f)
        {
            playing = false;
            GameManager.Instance.GameOver();
        }
        
        if(playing){
  
            timer -= Time.deltaTime;
            
            if (timer <= 0f)
            {
                timer = 0.0f;
            }
            
            //int minutes = Mathf.FloorToInt(timer / 60F);
            int seconds = Mathf.FloorToInt(timer % 60F);
           //int milliseconds = Mathf.FloorToInt((timer * 100F) % 100F);
            
            timerText.text = /*minutes.ToString ("00") + ":" + */"Time : " + seconds.ToString ("00")/* + ":" + milliseconds.ToString("00")*/;
        }
    }
}
