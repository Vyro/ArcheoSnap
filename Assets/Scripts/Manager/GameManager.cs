﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject gameOverUi;
    [SerializeField] private GameObject victoryUi;
    private bool gameHasEnded;
    private static GameManager instance;
    private LevelManager levelManager;

    public static GameManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<GameManager>();
                if (instance == null)
                {
                    instance = new GameObject().AddComponent<GameManager>();
                }
            }

            return instance;
        }
    }

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        //DontDestroyOnLoad(gameObject);
        
        levelManager = FindObjectOfType<LevelManager>();
        if (levelManager == null)
        {
            Debug.LogError("No levelManager");
        }
    }

    public void GameOver()
    {
        if (!gameHasEnded && !levelManager.IsLevelDone())
        {
            gameHasEnded = true;
            // Game Over
            Debug.Log("Game Over");
            gameOverUi.SetActive(true);
            Time.timeScale = 0f;
        }
    }

    public void Victory()
    {
        Debug.Log("Victory !");
        if (!gameHasEnded && levelManager.IsLevelDone())
        {
            gameHasEnded = true;
            Debug.Log("Level Done !");
            victoryUi.SetActive(true);
            Time.timeScale = 0f;
        }
    }

    public void NextLevel()
    {
        Debug.Log("Next level");
    }

    public void Restart()
    {
        SceneManager.LoadScene(0);
        Time.timeScale = 1f;
        gameHasEnded = false;
        Debug.Log("Restart");
    }
    
    public void Menu()
    {
        Debug.Log("Menu");
    }
}
