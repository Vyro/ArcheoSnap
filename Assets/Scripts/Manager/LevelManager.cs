﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    [SerializeField] private int nbBonesToStore;
    [SerializeField] private int nbBonesStored;
    private bool isLevelDone;

    public int GetNbBonesToStore()
    {
        return nbBonesToStore;
    }

    public bool IsLevelDone()
    {
        return isLevelDone;
    }

    public void SetIsLevelDone(bool isLevelDone)
    {
        this.isLevelDone = isLevelDone;
    }

    public int GetNbBonesStored()
    {
        return nbBonesStored;
    }

    public void SetNbBonesStored(int nb)
    {
        nbBonesStored = nb;
    }
}
