﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Dust : MonoBehaviour
{
    public GameObject dustPlane;
    public float radius;
    public float fossileRadius;
    public Color dustColor;

    private Mesh dustMesh;
    private List<Vector3> verticles = new List<Vector3>();
    private Color[] _colors;

    public string sceneName;
    
    private GameObject currentPlane;

    public List<GameObject> fossiles = new List<GameObject>();
    public Mesh[] fossilesMeshes;

    public Text timerText;
    public float timer;
    private float _timer;
    
    void Start()
    {
        Initialize(dustPlane);
        
        fossilesMeshes = new Mesh[fossiles.Count];
        
        for (int i = 0; i < fossiles.Count; i++)
        {
            fossilesMeshes[i] = fossiles[i].GetComponent<Collider2D>().CreateMesh(true, true);
            
            /*GameObject fossilePlaneObject = new GameObject();
            MeshFilter meshFilter = fossilePlaneObject.AddComponent<MeshFilter>();
            fossilePlaneObject.AddComponent<MeshRenderer>();
            meshFilter.mesh = fossilesMeshes[i];*/
        }

        _timer = timer;
        timerText.text = "Timer : " + (int) _timer;
    }
    
    void Update()
    {
        _timer -= Time.deltaTime;
        timerText.text = "Timer : " + (int) _timer;

        if (_timer <= 0)
        {
            GameManager.Instance.GameOver();
        }
        
        #region removeDust

         if (Input.GetButton("Fire1"))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            
            if (Physics.Raycast(ray, out hit, 1000))
            {
                for (int i = 0; i < verticles.Count; i++)
                {
                    Vector3 v = dustPlane.transform.TransformPoint(verticles[i]);
                    float distance = Vector3.SqrMagnitude(v - hit.point);

                    if (distance < radius)
                    {
                        float alpha = 0.0f;//Mathf.Min(_colors[i].a, distance / radius);
                        _colors[i].a = alpha;
                    }
                }

                //Initialize();
                UpdateColor();
                CheckWin();
            }

            for (int i = 0; i < fossiles.Count; i++)
            {
                int u = 0;
                
                int[] indexs = new int[fossilesMeshes[i].vertexCount];
                
                for (int j = 0; j < indexs.Length; j++)
                {
                    for (int k = 0; k < verticles.Count; k++)
                    {
                        //Debug.Log(verticles[k] + " =? " + new Vector3((int) fossilesMeshes[i].vertices[j].x, 0, (int) fossilesMeshes[i].vertices[j].z));

                        if (verticles[k] == new Vector3((int) fossilesMeshes[i].vertices[j].x, 0, (int) fossilesMeshes[i].vertices[j].z))
                        {
                            //Debug.Log("Found index");
                            indexs[j] = k;
                        }
                    }

                    if (_colors[indexs[j]].a == 0f)
                    {
                        u++;
                    }
                }

                if (u == indexs.Length)
                {
                    Debug.Log("Fossile Found");
                    //Destroy(fossiles[i]);
                }
                
                if (Vector2.Distance(fossiles[i].transform.position, Camera.main.ScreenToWorldPoint(Input.mousePosition)) <= fossileRadius)
                {
                    GameObject fos = fossiles[i];
                    fossiles.Remove(fos);
                    Destroy(fos);
                }
            }
            
            /*RaycastHit[] hits;
            RaycastHit hitPlane = new RaycastHit();
            
            hits = Physics.RaycastAll(ray, Mathf.Infinity);

            for (int i = 0; i < hits.Length; i++)
            {
                RaycastHit hit1 = hits[i];

                Mesh hitMesh = hit1.collider.gameObject.GetComponent<MeshFilter>().mesh;

                int j = 0;

                for (int k = 0; k < hitMesh.colors.Length; k++)
                {
                    if (hitMesh.colors[k].a == 0)
                    {
                        j++;
                    }
                }

                //Plane Done
                if (j != hitMesh.colors.Length)
                {
                    currentPlane = hit1.collider.gameObject;
                    hitPlane = hit1;
                    Initialize(currentPlane);
                    break;
                }

                if (i == hits.Length - 1)
                {
                    Win();
                }
            }

            for (int i = 0; i < verticles.Count; i++)
            {
                Vector3 v = dustPlane.transform.TransformPoint(verticles[i]);
                float distance = Vector3.SqrMagnitude(v - hitPlane.point);

                if (distance < radius)
                {
                    float alpha = 0.0f;
                    _colors[i].a = alpha;
                }
            }
            
            UpdateColor();*/
        }

        #endregion
    }

    public void Initialize(GameObject plane)
    {
        dustMesh = plane.GetComponent<MeshFilter>().mesh;
        verticles = dustMesh.vertices.ToList();
        _colors = new Color[verticles.Count];

        for (int i = 0; i < _colors.Length; i++)
        {
            _colors[i] = dustColor;
        }
        UpdateColor();
    }

    public void UpdateColor()
    {
        dustMesh.colors = _colors;
    }

    public void DeleteVerticles(List<Vector3> verticles)
    {
        for (int i = 0; i < verticles.Count; i++)
        {
            verticles.Remove(verticles[i]);
                        
            int[] newTriangles = new int[verticles.Count / 3];
                        
            for (i = 0; i < verticles.Count;  i += 3)
            {  
                newTriangles[i] = dustMesh.triangles[i];
                newTriangles[i + 1] = dustMesh.triangles[i + 1];
                newTriangles[i + 2] = dustMesh.triangles[i + 2];
            }
                        
            dustMesh.SetTriangles(newTriangles, 0, false);
                        
            dustMesh.vertices = verticles.ToArray();
                        
            dustMesh.RecalculateBounds();
        }
    }

    public void CheckWin()
    {
        int j = 0;
        
        for (int i = 0; i < _colors.Length; i++)
        {
            if (_colors[i].a == 0)
            {
                j++;
            }
        }

        int u = 0;

        for (int i = 0; i < fossiles.Count; i++)
        {
            if (!fossiles[i])
            {
                u++;
            }
        }

        if (u == fossiles.Count)// && j == _colors.Length)
        {
            Win();
        }
    }

    public void Win()
    {
        SceneManager.LoadScene(sceneName);
    }
}
